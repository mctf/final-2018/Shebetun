CREATE DATABASE yii2basic;

USE yii2basic;

CREATE TABLE `tweet` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `owner_id` INT NOT NULL,
        `title` TEXT NOT NULL,
        `text` TEXT NOT NULL,
        `date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'public',
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE USER 'admin'@'%' IDENTIFIED BY 'haT3th1SsuPp0rT';

GRANT INSERT,SELECT,UPDATE ON `yii2basic`.`user` TO 'admin'@'%';
GRANT INSERT,SELECT,UPDATE ON `yii2basic`.`tweet` TO 'admin'@'%';

FLUSH PRIVILEGES;