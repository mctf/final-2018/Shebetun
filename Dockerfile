FROM ubuntu:16.04

WORKDIR /var/www/public

RUN apt-get update
RUN apt-get install -y \
    zip \
    unzip \
    git \
    curl \
    apache2 \
    php \
    php-mbstring \
    php-dom \
    php-gd \ 
    php-mysql \
    libapache2-mod-php \
    mysql-client \
    php-xdebug && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./application/ /var/www/public/
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY apache2-foreground /usr/local/bin/

RUN chmod 744 /usr/local/bin/apache2-foreground
RUN chown -hR www-data:www-data /var/www/public/

RUN a2enmod headers

EXPOSE 80

CMD ["apache2-foreground"]
