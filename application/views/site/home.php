<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>Tweets</h1>

<p>
    Here all your tweets and news are listed, click on them to read.
</p>

<ul>
<?php foreach ($tweets as $tweet): ?>

    <a href="/index.php?r=tweet%2Findex&id=<?= $tweet['id'] ?>" class="tweet">
		<b><?= Html::encode("{$tweet['title']}") ?></b>
        <br>
        <p><?= Html::encode(substr($tweet['text'],0 , 10). (strlen($tweet['text']) > 10 ? '...' : '')) ?></p>
    </a>
<?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>