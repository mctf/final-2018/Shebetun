<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
 


$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Settings</h1>

<?php if($user->file){ ?>
	<img src="<?='/images/'.$user->file?>" class="profile_image" />
<?php } ?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'form-upload']) ?>

    <?= $form->field($model, 'id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => $user->username, 'value' => '']) ?>
    <?= $form->field($model, 'email')->textInput(['placeholder' => $user->email, 'value' => '']) ?>
    <?= $form->field($model, 'password')->passwordInput(['placeholder' => '********', 'type' => 'password', 'value' => '']) ?>
    <?= $form->field($model, 'type')->checkbox([ 'value' => 'private', 'uncheck' => 'public' , 'checked ' => $user->type == 'public' ? false : true])->label('Pivate profile'); ?>
    <?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'upload-button']) ?>
    </div>

<?php ActiveForm::end() ?>