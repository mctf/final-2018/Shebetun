<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\models\User;

$this->title = 'Shebetun';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>
        
        <p class="lead">in a social network where everyone can publish posts, and can keep the information private.</p>

        <p></p>
    </div>

    <div class="body-content">

        <div class="row">
        <?php
        if (Yii::$app->user->isGuest) {
        ?>
            <div class="col-lg-4">
                <h2>Read user tweets</h2>

                <p>Users with public accounts will share their messages with you.</p>
            </div>
            <div class="col-lg-4">
                <h2>Publish your stories</h2>

                <p>People will read your posts, share stories and events..</p>
            </div>
            <div class="col-lg-4">
                <h2>Post information about only for yourself</h2>

                <p>Post information only for yourself, then watch your story.</p>
            </div>
        <?php
        } else {
        ?>
            <div class="">
                <h2>Read tweets of Users:</h2>
                <br/>
                <div class="column">
                <?php foreach ($users as $user): ?>
                    <p style="font-size: 24px"><?= Html::encode("{$user->username}") ?> <a class="btn btn-default" href="index.php?r=tweets%2Findex&owner=<?=$user->id?>"><b>Читать</b></a></p>
                <?php endforeach; ?>
                </div>
            </div>
            <?= LinkPager::widget(['pagination' => $pagination]) ?>
        <?php
        }
        ?>
        </div>

    </div>
</div>

