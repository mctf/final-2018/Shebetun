<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Tweet';
$this->params['breadcrumbs'][] = $this->title;?>
<h1>Tweet</h1>

<div class="tweet_full">
    <h4><?= Html::encode("{$owner['username']}") ?></h4>
    
    <p><?= Html::encode("{$tweet['title']}") ?></p>
    
    <p><?= Html::encode("{$tweet['text']}") ?></p>

    <p align="right">Post date: <?= $tweet['date'] ?></p>
</div>