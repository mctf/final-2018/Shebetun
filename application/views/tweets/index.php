<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Tweets</h1>


<?php 
    if(!empty($user['file'])){ 
        echo '<img src="' . '/images/' . $user['file'] . '" class="profile_image" />';
    }
    
    echo '<p class="username" >' . $user['username'] .'</p>';
?>

<ul>
<?php foreach ($tweets as $tweet): ?>
    <a href="/index.php?r=tweet%2Findex&id=<?= $tweet['id'] ?>" class="tweet">
        <b><?= Html::encode("{$tweet['title']}") ?></b>
        <br>
        <p><?= Html::encode(substr($tweet['text'],0 , 10).(strlen($tweet['text']) > 10 ? '...' : '')) ?></p>
    </a>
<?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>