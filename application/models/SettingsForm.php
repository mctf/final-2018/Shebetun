<?php
 
namespace app\models;
 
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Settings form
 */
class SettingsForm extends Model
{
 
    public $id;
    public $username;
    public $email;
    public $password;
    public $type;
    public $file;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'number'],
            ['username', 'trim'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 3, 'max' => 255],
            ['username', 'match', 'pattern' => '/^[a-zA-Z0-9\-\_]{3,255}$/u'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'string', 'min' => 4],
            ['type', 'required'],
            ['type', 'string'],
            ['file', 'file']
        ];
    }
 
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function Settings()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = User::findOne($this->id);

        if ($this->username !== Yii::$app->user->identity->username && !empty($this->username)){
            $user->username = $this->username;
        }

        if ($this->email !== Yii::$app->user->identity->email && !empty($this->email)){
            $user->email = $this->email;
        }
        
        if ($this->password) {
            $user->setPassword($this->password);
        }

        $user->type = $this->type == 'private' ? 'private' : 'public' ;

        if (!empty($this->file)){

            $baseName = md5(time().$this->file->baseName);

            if ($this->file->saveAs(Yii::getAlias('@images/') . $baseName . '.' . $this->file->extension)){

                if ($user->file){
                    unlink(Yii::getAlias('@images/') . $user->file);
                }

                $user->file = $baseName . '.' . $this->file->extension;

            }
        }

        $user->generateAuthKey();
        return $user->save() ? $user : null;
    }
}