<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * tweetForm is the model behind the tweet form.
 */
class TweetForm extends Model
{
    public $subject;
    public $body;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['subject', 'body'], 'required']
        ];
    }

    // /**
    //  * @return array customized attribute labels
    //  */
    // public function attributeLabels()
    // {
    //     return [
    //         'verifyCode' => 'Verification Code',
    //     ];
    // }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function tweet()
    {
          if (!$this->validate()) {
            return null;
        }
 
        $tweet = new Tweet();
        $tweet->owner_id = Yii::$app->user->getId();
        $tweet->title = $this->subject;
        $tweet->text = $this->body;
        return $tweet->save() ? $tweet : null;
    }
}
