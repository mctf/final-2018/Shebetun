<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Tweet;
use app\models\User;


class TweetController extends Controller
{
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            $tweet = Yii::$app->db->createCommand("SELECT * FROM tweet WHERE id=".Yii::$app->request->get('id'))
                ->queryOne();

            $owner = Yii::$app->db->createCommand("SELECT * FROM user WHERE id=".$tweet['owner_id'])
                ->queryOne();

            return $this->render('index', [
                'tweet' => $tweet,
                'owner' => $owner
            ]);
        }
    }
}
