<?php

namespace app\controllers;

use Yii;
use app\models\TweetForm;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\SettingsForm;
use app\models\User;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\Response;
use app\models\Tweet;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            $query = User::find()->where(['type' => 'public']);;

            $pagination = new Pagination([
                'defaultPageSize' => 10,
                'totalCount' => $query->count(),
            ]); 

            $users = $query->orderBy(['created_at' => SORT_DESC])
                ->where([
                    'type' => 'public'
                ])
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->orderBy('id DESC')
                ->all();

            return $this->render('index', [
                'users' => $users,
                'pagination' => $pagination,
            ]);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionTweet()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new TweetForm();
        if ($model->load(Yii::$app->request->post()) && $model->tweet()) {
            Yii::$app->session->setFlash('tweetFormSubmitted');

            return $this->refresh();
        }
        return $this->render('tweet', [
            'model' => $model,
        ]);
    }

    /**
     * Displays user Tweets page.
     *
     * @return string
     */
    public function actionHome()
    {
        if (Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            $query = Tweet::find()->where(['owner_id'=>Yii::$app->user->id]);

            $pagination = new Pagination([
                'defaultPageSize' => 5,
                'totalCount' => $query->count(),
            ]); 

            $tweets = Yii::$app->db->createCommand("SELECT * FROM tweet WHERE owner_id=" .
                Yii::$app->user->id .
                " ORDER BY id DESC LIMIT " . $pagination->offset . "," . $pagination->limit
            )->queryAll();

            $user = Yii::$app->db->createCommand("SELECT * FROM user WHERE id=".
                Yii::$app->user->id
            )->queryOne();

            return $this->render('home', [
                'tweets' => $tweets,
                'pagination' => $pagination,
                'user' => $user
            ]);
        }
    }

        /**
     * Displays user Tweets page.
     *
     * @return string
     */
    public function actionSettings()
    {
        if (Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {

            $model = new SettingsForm();
            
            if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
                $model->file = UploadedFile::getInstance($model, 'file');
                
                if ($user = $model->settings()) {
                    // file is uploaded successfully
                    return $this->render('settings', [
                        'model' => $model,
                        'user' => $user
                    ]);;
                }
            }

        
            return $this->render('settings', [
                'model' => $model,
                'user' => Yii::$app->user->identity
            ]);
        }
    }
    

    /**
     * Register user.
     *
     * @return string
     */
    public function actionSignup()
    {
        $model = new SignupForm();
 
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
 
        return $this->render('signup', [
            'model' => $model
        ]);
    }
}
