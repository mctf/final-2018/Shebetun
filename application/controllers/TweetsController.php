<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Tweet;
use app\models\User;

class TweetsController extends Controller
{
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            $query = Tweet::find()->where(['owner_id'=>Yii::$app->request->get('owner')]);

            $pagination = new Pagination([
                'defaultPageSize' => 10,
                'totalCount' => $query->count(),
            ]); 

            $tweets = Yii::$app->db->createCommand("SELECT * FROM tweet WHERE owner_id=" .
                Yii::$app->request->get('owner') .
                " LIMIT " . $pagination->offset . "," . $pagination->limit
            )->queryAll();

            $user = Yii::$app->db->createCommand("SELECT * FROM user WHERE id=".
                Yii::$app->request->get('owner')
            )->queryOne();

            return $this->render('index', [
                'tweets' => $tweets,
                'pagination' => $pagination,
                'user' => $user
            ]);
        }
    }
}
